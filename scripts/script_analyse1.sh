conda install fastqc
conda install cutadapt
conda install star
conda install multiqc

export WR=$(pwd)
cd $WR
mkdir -p res/genome
cd res/genome
wget -O ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz 
gunzip -k ecoli.fasta.gz
cd $WR
mkdir -p res/genome/star_index
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fasta --genomeSAindexNbases 9
cd $WR
bash scripts/script_analyse2.sh
